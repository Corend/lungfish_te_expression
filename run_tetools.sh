# Create the fasta of the copies of the TEs

## Variables
Species="Lungfish"
Home="/media/disk1/cdechaud/Lungfish/TEtools/data/tetools_home"
InsertSize="262"
Column="2"
FDR="0.01"
#SamplesName="sample1:Ovary,sample2:Ovary,sample3:Ovary,sample4:Testis,sample5:Testis,sample6:Testis"
#Formula="sample:condition"
#SamplesName="Ovary:1,Ovary:2,Ovary:3,Testis:4,Testis:5,Testis:6"
SamplesName="Brain:1,Gonads:2,Liver:3"
Formula="condition:replicat"


## Files
# Only TEs.bed
TEucsc="/media/disk1/cdechaud/Lungfish/TEtools/data/annotation/NFOR_v3.1.repeatmasker.sub.out.ucsc"
Rosette="${Home}/${Species}.rosette"
# Genome
Genome="/media/disk1/cdechaud/Lungfish/TEtools/data/genome/NFOR_v3.1.RNAseq_correction_upper.fa"
# Reads
Readir="/media/disk1/cdechaud/Lungfish/TEtools/data/fastq/"
Reads1="${Readir}Brain_PolyA_trimmed_R1.fq ${Readir}Gonads_PolyA_trimmed_R1.fq ${Readir}Liver_PolyA_trimmed_R1.fq"
Reads2="${Readir}Brain_PolyA_trimmed_R2.fq ${Readir}Gonads_PolyA_trimmed_R2.fq ${Readir}Liver_PolyA_trimmed_R2.fq"
OUTSTAT="${Home}/${Species}_StatOut_${Column}"

# Fasta of copies
TEfa="${Home}/${Species}_TEcopies.fa"

# Output from TEcount
TEcount="${Home}/${Species}_TEcount_${Column}.csv"

## Create bed of TEs:
if [[ ! -f ${TEucsc}.bed ]]; then
less $TEucsc |sed 's/?//g'|awk 'BEGIN {OFS=FS="\t"}{print $6, $7-1, $8, $11}' |sed 's/chr//g;1d' > $TEucsc.bed
fi

## Create the fasta of copies:
if [[ ! -f ${TEfa} ]]; then
echo "Generating the fasta file of TE copies ..."
bedtools getfasta -fi $Genome -bed $TEucsc.bed -fo $TEfa -name
awk '(/^>/ && s[$0]++){$0=$0"~"s[$0]}1;' $TEfa > $TEfa.tmp
mv $TEfa.tmp $TEfa
less $TEfa |awk '{if(substr($0,1,1)==">"){if((index($0, "~") == 0)){print $0"~"}else{print $0}}else{print $0}}' > $TEfa.tmp
mv $TEfa.tmp $TEfa 
echo "Generation of the fasta file of the TE copies is over."
fi

## Create the Rosette file.
if [[ ! -f ${Rosette} ]]; then
grep ">" $TEfa |awk 'BEGIN{OFS="\t"; FS=">|~"}{print $2"~"$3,$2}' |sort -k2,2 >$TEfa.table
awk 'BEGIN{OFS="\t"}{print $11, $12, $13}' $TEucsc |sed '1d' |sort -k1,1|uniq>$TEfa.table.end
join $TEfa.table $TEfa.table.end -1 2 -2 1 | awk 'BEGIN{OFS="\t"}{print $2, $1, $4, $3}'> $Rosette
rm $TEfa.table
rm $TEfa.table.end
fi

# Count TE expression
# TEcount.py -rosette [$]ROSETTE_FILE] -column [$]COUNT_COLUMN] -TE_fasta [FASTA_FILE] -count [OUTPUT_FILE] -RNA [FASTQ_FILE1 FASTQ_FILE2 ... FASTQ_FILEN]

# First using samples:
if [[ ! -f ${TEcount} ]]; then
/media/disk1/cdechaud/Lungfish/TEtools/TEtools/TEcount.py -rosette ${Rosette} -column ${Column} -TE_fasta $TEfa -count ${TEcount} -RNA $Reads1 -RNApair $Reads2 -insert $InsertSize 
fi

Rscript /media/disk1/cdechaud/Lungfish/TEtools/TEtools/TEdiff.R --args --FDR_level=${FDR} --count_column=4 --count_file=\"${TEcount}\" experiment_formula=\"${Formula}\" --sample_names=\"${SamplesName}\" --outdir=\"${OUTSTAT}\" --htmlfile=\"${TEcount}.html\"
